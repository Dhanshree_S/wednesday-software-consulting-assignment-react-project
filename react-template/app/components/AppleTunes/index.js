import React from 'react';
import { useEffect, useState } from 'react';
import axios from 'axios';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'bootstrap/dist/css/bootstrap.min.css';

function index() {
  let [cakesresult, setCakes] = useState([]);
  let search = function(event) {
    event.preventDefault();
    alert();
    let SearchText = document.getElementById('txtSearch').value;
    console.log('url===>', SearchText);

    let searchCakeApi = 'https://itunes.apple.com/search?term=' + SearchText;
    axios({
      method: 'get',
      url: searchCakeApi
        }).then(
      response => {
        setCakes(response.data.results);
        console.log('songs', response);
        alert('response');
      },
      error => {
        console.log('erro from cake api', error);
      }
    );
  };

  return (
    <div className="container">
      <div className="row">
        <h2 className="p-2 my-4 text-center">Hello.. Welcome to the iTunes</h2>
        <div className="text-center">
          <input
            className="mr-sm-2 col-6 h-100 mr-2"
            type="search"
            placeholder="Search"
            aria-label="Search"
            id="txtSearch"
          ></input>
          <button onClick={search} className="col-2 btn btn-outline-dark ml-2" type="submit">
            Search
          </button>
        </div>
      </div>
      <div className="container  py-5">
        <div className="row">
          {cakesresult?.length > 0 ? (
            cakesresult.map((each, index) => {
              return (
                <div className="col-md-4 mb-4">
                  <div className="border p-2">
                    <h4>{each.trackName}</h4>
                    <h4>{each.artistName}</h4>
                    <p>{each.collectionName}</p>
                    <audio controls>
                      <source src={each.previewUrl} type="audio/ogg"></source>
                      <source src={each.previewUrl} type="audio/mpeg"></source>
                    </audio>
                  </div>
                </div>
              );
            })
          ) : (
            <div className="col-12 mx-auto text-center ">
              <p className="alert alert-info ">No result found for search, Please try other cakes...</p>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
export default index;
